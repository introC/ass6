a.out: main.o dictionary.o functions.o 
	gcc -std=c99 main.o dictionary.o functions.o genericBinTree.o 
main.o: main.c
	gcc -c -std=c99 main.c
dictionary.o: dictionary.c dictionary.h
	gcc -c -std=c99 dictionary.c
functions.o: functions.c functions.h
	gcc -c -std=c99 functions.c
clean:
	rm -f main.o dictionary.o functions.o genericBinTree.o  a.out
run: a.out
	./a.out