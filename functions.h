//
// Created by User on 16/01/2020.
//

#ifndef ASS6_FUNCTIONS_H
#define ASS6_FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include "genericBinTree.h"
#include "entry.h"

Ptr allocate_entry();
void destroy_entry(Ptr ptr);
void copy_entry(Ptr ptr1, Ptr ptr2);
int compare_entry(Ptr ptr1, Ptr ptr2);
void print_entry(Ptr ptr);

#endif //ASS6_FUNCTIONS_H
