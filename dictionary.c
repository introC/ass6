/*****************
 * Yair Shpitzer
 * 313285942
 * 01
 * ass6
 ***************/

#include <stdlib.h>
#include <stdio.h>
#include "dictionary.h"
#include "genericBinTree.h"
#include "functions.h"

struct Dictionary {
    struct BinTree *root;
};

BinTreeFunctions funcs = {allocate_entry, destroy_entry, copy_entry, compare_entry, print_entry};

/********************************************************************************************************
 * Function Name: initDictionary
 * Input: none
 * Output: Dictionary*
 * Function Operation: the function allocates memory for a dictionary and creates it's field (BinTree)
 *******************************************************************************************************/
Dictionary *initDictionary() {
    Dictionary *dictionary = (Dictionary *) malloc(sizeof(Dictionary));
    if (dictionary == NULL){
        printf("error!\n");
        return NULL;
    }
    dictionary->root = createBinTree(funcs);
    return dictionary;
}

/**********************************************************************************************************
 * Function Name: destroyDictionary
 * Input: Dictionary* d
 * Output: void
 * Function Operation: the function get a pointer to dictionary and destroys all of it's allocated memory
 *********************************************************************************************************/
void destroyDictionary(Dictionary *d) {
    destroyBinTree(d->root);
    free(d);
}

/********************************************************************************************************
 * Function Name: sizeOfDictionary
 * Input: Dictionary* d
 * Output: int
 * Function Operation: the function get a pointer to dictionary and calculates it's size (number of keys)
 *******************************************************************************************************/
int sizeOfDictionary(Dictionary *d) {
    return sizeOfBinTree(d->root);
}

/******************************************************************************************************************
 * Function Name: putInDictionary
 * Input: Dictionary* d, int key, int value
 * Output: Result
 * Function Operation: the function gets key and value, creates an Entry with them, and put it in the dictionary.
 *                     if the key already exists, it's value is updated to the new value
 *****************************************************************************************************************/
Result putInDictionary(Dictionary *d, int key, int value) {
    Entry *entry = (Entry *) malloc(sizeof(Entry *));
    if (entry == NULL){
        printf("error!\n");
        return MEM_ERROR;
    }
    entry->key = key;
    entry->value = value;
    Ptr newVal = (Ptr) entry;
    Result res;

    BinTree *findLeaf = findInBinTree(d->root, newVal);
    // if findLeaf isn't NULL, it's already in the dictionary. in this case, the new value should replace the old one
    if (findLeaf != NULL) {
        Ptr oldVal = createCopyOfValueInRoot(findLeaf);
        removeFromBinTree(&(d->root), oldVal);
        res = addToBinTree(d->root, newVal);
        free(oldVal);
        free(entry);
        return res;
    }
    // otherwise add the new entry to dictionary
    res = addToBinTree(d->root, newVal);
    free(entry);
    return res;
}

/******************************************************************************************************************
 * Function Name: getFromDictionary
 * Input: Dictionary* d, int key
 * Output: int
 * Function Operation: the function gets a key, and search for it in the dictionary. if found, returns it's value.
 *                     otherwise, returns 0.
 *****************************************************************************************************************/
int getFromDictionary(Dictionary *d, int key) {
    Entry *entry = (Entry *) malloc(sizeof(Entry *));
    if (entry == NULL){
        printf("error!\n");
        return 0;
    }
    entry->key = key;
    Ptr treeVal = (Ptr) entry;

    BinTree *findLeaf = findInBinTree(d->root, treeVal);
    // if findLeaf isn't NULL, it's already in the dictionary. in this case, return the value
    if (findLeaf != NULL) {
        Ptr temp = createCopyOfValueInRoot(findLeaf);
        Entry *entryVal = (Entry*) temp;
        int resultVal = entryVal->value;
        free(entry);
        free(temp);
        return resultVal;
    }
    free(entry);
    return 0;
}

/*********************************************************************************************
 * Function Name: removeFromDictionary
 * Input: Dictionary* d, int key
 * Output: Result
 * Function Operation: the function gets key and search for it in dictionary.
 *                     if found, it removes the entry from dictionary. otherwise, return 0.
 ********************************************************************************************/
Result removeFromDictionary(Dictionary *d, int key) {
    Entry *entry = (Entry *) malloc(sizeof(Entry *));
    if (entry == NULL){
        printf("error!\n");
        return MEM_ERROR;
    }
    entry->key = key;
    Ptr treeVal = (Ptr) entry;

    BinTree *findLeaf = findInBinTree(d->root, treeVal);
    // if findLeaf isn't NULL, it's already in the dictionary. in this case, remove the entry
    if (findLeaf != NULL) {
        Ptr oldVal = createCopyOfValueInRoot(findLeaf);
        removeFromBinTree(&(d->root), oldVal);
        free(oldVal);
        free(entry);
        return SUCCESS;
    }
    free(entry);
    return FAILURE;
}

/*********************************************************************************************************
 * Function Name: printDictionary
 * Input: Dictionary* d
 * Output: void
 * Function Operation: the function gets a pointer to dictionary and prints it according to instructions
 ********************************************************************************************************/
void printDictionary(Dictionary *d) {
    printf("{");
    printInOrder(d->root);
    printf("}");
}

/*********************************************************************************************************************
 * Function Name: createDictionaryFromArrays
 * Input: int keys[], int values[], int size
 * Output: Dictionary*
 * Function Operation: the function gets two arrays, one of keys and one of values. also the function gets the size of
 *                     both arrays (they are from the same length). the function creates an array of entries
 *                     and creates a dictionary from the entries array.
 ********************************************************************************************************************/
Dictionary *createDictionaryFromArrays(int keys[], int values[], int size) {
    Dictionary* d = initDictionary();
    Result res;
    for (int i = 0; i < size; i++){
        res = putInDictionary(d, keys[i], values[i]);
        if (res != SUCCESS){
            destroyDictionary(d);
            return NULL;
        }
    }
    return d;
}