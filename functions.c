#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

Ptr allocate_entry() {
    return malloc(sizeof(Entry *));
}

void destroy_entry(Ptr p) {
    free(p);
}

void copy_entry(Ptr p1, Ptr p2) {
    Entry *temp1 = (Entry *) p1;
    Entry *temp2 = (Entry *) p2;
    temp1->value = temp2->value;
    temp1->key = temp2->key;
}

int compare_entry(Ptr p1, Ptr p2) {
    Entry *temp1 = (Entry *) p1;
    Entry *temp2 = (Entry *) p2;
    return temp1->key - temp2->key;
}

void print_entry(Ptr p) {
    Entry *temp = (Entry *) p;
    printf("[%d:%d]", temp->key, temp->value);
}